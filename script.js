// Assign Elements
const screenHistory = document.querySelector("#history");
const screenNumber = document.querySelector("#number");

const buttons = document.querySelectorAll(".btn");


// Set Variables
let history = [];
let screenCurrent = "0";
let screenOperation = "";
let prev = 0;
let current = 0;
let operation;
let result;
let isNewNumber;
let isAfterEquals = false;
let isAfterChangeOperation = false;


// Functions

// Displays
function displayCurrent() {
    screenNumber.innerHTML = screenCurrent;
}

function displayHistory(isEquals) {
    screenHistory.innerHTML = history.join(" ");
}

function clearAll() {
    screenCurrent = "0"
    screenOperation = "";
    history = [];
    prev = 0;
    current = 0;
    operation  = "";
    result = 0;
    isNewNumber = true;
    isAfterEquals = false;
    displayHistory();
    displayCurrent();
}

// Compute
function compute() {
    console.log(prev,current, result, operation);
    switch(true) {
        case operation == "plus":
            result = prev + current;
            break;
        case operation == "minus":
            result = prev - current;
            break;
        case operation == "times":
            result = prev * current;
            break;
        case operation == "divide":
            result = prev / current;
            break;
    }
}


function handleNumberInput(num) {
    if(isAfterEquals) {
        clearAll();
    }
    if(screenCurrent == "0" || isNewNumber) {
        screenCurrent = num;
        current = parseFloat(num);
    } else {
        screenCurrent += parseInt(num)
        current = parseFloat(screenCurrent);
    }
    displayCurrent();
    isNewNumber = false;
    isAfterEquals = false
}

function handlePointInput() {
    if(!screenCurrent.includes(".")) {
        (screenCurrent += ".")
        displayCurrent();
    }
}

function handleNegate() {
    screenCurrent = "" + (parseInt(screenCurrent) * -1);
    current = parseFloat(screenCurrent);
    displayCurrent();
    isNewNumber = false;
}

function handleDelete() {
    if(screenCurrent.length > 1) {
        screenCurrent = screenCurrent.slice(0,-1);
    } else {
        screenCurrent = "0"
    }
    displayCurrent();
}

function handleClearEntry() {
    screenCurrent = "0";
    displayCurrent();
}

function handleClearAll() {
    clearAll();
}

function handleOperationInput(selectedOperation) {
    
    function getNewOperation() {
        switch(selectedOperation) {
            case "plus":
                operation = "plus";
                screenOperation = "&plus;";
                break;
            case "minus":
                operation = "minus";
                screenOperation = "&minus;";
                break;
            case "times":
                operation = "times";
                screenOperation = "&times;";
                break;
            case "divide":
                operation = "divide";
                screenOperation = "&divide;";
                break;
        }
    }

    if(!isAfterEquals) {
        if(!operation) {
            prev = current;
            getNewOperation();
            history.push(prev, screenOperation);
        } else {

            if(isAfterChangeOperation) {
                if(operation == selectedOperation) {
                    compute();
                    history = [prev, screenOperation, current];
                } else {
                    getNewOperation();
                    history = [prev, screenOperation];
                }
                console.log(prev, current, result);
                displayHistory();
                prev = result;
                screenCurrent = result;
                displayCurrent();
                console.log(prev, current, result);
            } else {
                prev = current;
                getNewOperation();
                history.pop();
                history.push(screenOperation);
            }
        }
    
        isNewNumber = true;
        displayHistory();
    } else {
        prev = result;
        history = [prev, screenOperation];
        isNewNumber = true;
        isAfterEquals = false;
        displayHistory();
    }

    isAfterChangeOperation = true;
}

function handleEquals() {
    if(operation) {
        // Process History
        history = history.filter(e => e !== "=");
        if(isAfterEquals) {
            prev = parseFloat(screenCurrent);
            history = [prev, screenOperation, current, "="];
        } else {
            history.push(current, "=")
        }
        displayHistory();
        compute();
        screenCurrent = result;
        displayCurrent();
    
        isAfterEquals = true;
    }
}

// Button Click
buttons.forEach(button => {
    button.addEventListener("click", function(e) {
        if(screenNumber.innerHTML.length < 21) {
            if(!isNaN(button.dataset.value)) {
                handleNumberInput(button.dataset.value);
            } else if(button.dataset.value == "point") {
                handlePointInput();
            } else if(button.dataset.value == "equals") {
                handleEquals();
            } else if(button.dataset.value == "delete") {
                handleDelete();
            } else if(button.dataset.value == "clearEntry") {
                handleClearEntry();
            } else if(button.dataset.value == "clearAll") {
                handleClearAll();
            } else if(button.dataset.value == "negate") {
                handleNegate();
            } else {
                handleOperationInput(button.dataset.value);
            }
        }
    }, false)
})